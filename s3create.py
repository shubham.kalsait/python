import boto3

svc = boto3.client('s3', region_name="ap-south-1")
list_buckets = svc.list_buckets()

# print(list_buckets)
# print("\n")
# print(list_buckets["Buckets"])

bucket_names = list_buckets["Buckets"]
# print('\n')
# print(bucket_names[0])
# print('\n')
# print(bucket_names[1])
# print('\n')

for bucket in bucket_names:
    name = bucket['Name']
    print(name)

