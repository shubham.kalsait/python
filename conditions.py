# Condition (if) TRUE then only execute

print("####### WELCOME TO MY CALCI WORLD ###########")
# num1 = 12
# num2 = 3

num1 = input("Enter value 1: ")
num2 = input("Enter value 2: ")

# if Condition: 
if num1 < num2:
    print(num2, "is greater than", num1)
# if num1 > num2:
#     print(num1,"is greater than", num2)
elif num1 > num2:
    print(num1, "is greater than", num2)
else: 
    print(num1, "is equal to", num2)
print("Hence proved")

